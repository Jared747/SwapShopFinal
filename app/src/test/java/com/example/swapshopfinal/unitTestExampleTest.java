package com.example.swapshopfinal;

import junit.framework.TestCase;

import org.junit.Assert;

public class unitTestExampleTest extends TestCase {

    private unitTestExample example = null;

    public void setUp() throws Exception {
        example = unitTestExample.getInstance();
    }

    public void tearDown() throws Exception {
        example = null;
    }

    public void testGetText() {
        Assert.assertEquals(example.getText(49), "low");
    }
}