package com.example.swapshopfinal;

public class unitTestExample {
    private static unitTestExample example = null;

    public static unitTestExample getInstance(){
        if(example == null){
            example = new unitTestExample();
        }
        return example;
    }

    public String getText(int progress){
        if (progress>=0 && progress < 50){
            return "low";
        }
        else{
            return "high";
        }
    }
}
